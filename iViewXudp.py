#!/usr/bin/python2

import socket

debuglogging = False

UDP_IP = "192.168.1.1"
UDP_PORT = 4444
MESSAGE = "ET_STP " +chr(10) + chr(13) + chr(0)


sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

def setDebugLogging(b):
    global debuglogging
    debuglogging = b


def connect(ip, port):
    global UDP_IP, UDP_PORT, sock, debuglogging
    UDP_IP = ip
    UDP_PORT = port
    if debuglogging:
        print "IP: ", UDP_IP, "Port: ", UDP_PORT


def startRecording():
    global UDP_IP, UDP_PORT, sock, debuglogging
    msg = "ET_REC " +chr(10) + chr(13) + chr(0)
    sock.sendto(msg, (UDP_IP, UDP_PORT))
    if debuglogging:
        print msg


def stopRecording():
    global UDP_IP, UDP_PORT, sock, debuglogging
    msg = "ET_STP " +chr(10) + chr(13) + chr(0)
    sock.sendto(msg, (UDP_IP, UDP_PORT))
    if debuglogging:
        print msg


def pauseRecording():
    global UDP_IP, UDP_PORT, sock, debuglogging
    msg = "ET_PSE " +chr(10) + chr(13) + chr(0)
    sock.sendto(msg, (UDP_IP, UDP_PORT))
    if debuglogging:
        print msg


def continueRecording():
    global UDP_IP, UDP_PORT, sock, debuglogging
    msg = "ET_CNT " +chr(10) + chr(13) + chr(0)
    sock.sendto(msg, (UDP_IP, UDP_PORT))
    if debuglogging:
        print msg


def newTrial():
    global UDP_IP, UDP_PORT, sock, debuglogging
    msg = "ET_INC " +chr(10) + chr(13) + chr(0)
    sock.sendto(msg, (UDP_IP, UDP_PORT))
    if debuglogging:
        print msg


def remark(s):
    global UDP_IP, UDP_PORT, sock, debuglogging
    msg = "ET_REM " + s +chr(10) + chr(13) + chr(0)
    sock.sendto(msg, (UDP_IP, UDP_PORT))
    if debuglogging:
        print msg


def bitmap(s):
    global UDP_IP, UDP_PORT, sock, debuglogging
    msg = "ET_BMP " + s +chr(10) + chr(13) + chr(0)
    sock.sendto(msg, (UDP_IP, UDP_PORT))
    if debuglogging:
        print msg


def saveToFile(s):
    global UDP_IP, UDP_PORT, sock, debuglogging
    msg = "ET_SAV " + s +chr(10) + chr(13) + chr(0)
    sock.sendto(msg, (UDP_IP, UDP_PORT))
    if debuglogging:
        print msg

