#!/usr/bin/env python2
# first part of this file describes the flask server that receives and saves the data
# the part in "main" starts the server and start the client
import json
import sys
import os
import subprocess
import urllib

from flask import Flask, send_file, request
from flask_debugtoolbar import DebugToolbarExtension

import iViewXudp as iview

app = Flask(__name__.split('.')[0])
toolbar = DebugToolbarExtension(app)

@app.route("/")
def hello():
	return "Hello World! Welcome to Cool."

@app.route("/static/save_data_file.php", methods = ['POST', 'GET']) # GET is just for testing, experiment uses POST
def data():
	r = request.values # both form and args
	ppn = r['ppn'] # may be an empty string
	data = r['data'] # json encoded dict including timestamp and ppn
	app.logger.info(data)
	with open("static/data/data_{:s}.dat".format(ppn), "a") as file:
		file.write(data.encode("utf-8") + ",\n")
	try:
		d = json.loads(data)
		if "trigger" in d:
			iview.bitmap(d["trigger"]) # always a filename in the stimulus directory, required for iviewx
			iview.remark(d["trigger"]) # typically a filename in the idf-file folder or subfolder, required for begaze
			app.logger.info("sending trigger '{:s}'".format(d["trigger"]))
	except Exception as e:
		app.logger.error("cannot parse json dictionary: {:s}".format(e))
	return data
	

if __name__ == "__main__":
	print("Starting Cool server")
	iview.connect("192.168.1.1", 4444)
	iview.setDebugLogging(True)
	iview.startRecording()


	if(len(sys.argv)<2 or sys.argv[1]!="--server"):
		# client is non-deamon, ending server will close browser
		print("Starting Cool client")
		firefox = r"C:\Program Files (x86)\Mozilla Firefox\firefox.exe"
		url = "http://localhost:5000/static/index.html"
		client = subprocess.Popen([firefox, '-no-remote', '-url', url],
			stdout=subprocess.PIPE,
			stderr=subprocess.PIPE)
	app.run(debug=True, use_reloader=False)
