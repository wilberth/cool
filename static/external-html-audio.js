/** (July 2012, Erik Weitnauer)
The html-plugin will load and display an external html pages. To proceed to the next, the
user might either press a button on the page or a specific key. Afterwards, the page get hidden and
the plugin will wait of a specified time before it proceeds.

documentation: docs.jspsych.org
*/
var trial, t0

jsPsych.plugins['external-html-audio'] = (function() {
	var plugin = {}
	// jsPsych.pluginAPI.registerPreload('external-html-audio', 'audio_url', 'audio') // slow, use only in production
	// three additional saveData calls are made in this object: space bar press, click and load

	plugin.info = {
		name: 'external-html-audio',
		description: '',
		parameters: {
			url: {
				type: jsPsych.plugins.parameterType.STRING,
				pretty_name: 'URL',
				default: undefined,
				description: 'The url of the external html page'
			},
			trigger: {
				type: jsPsych.plugins.parameterType.OBJECT,
				pretty_name: 'trigger',
				default: {trigger: "trigger"},
				description: 'the dictionary sent as trigger after loading the screen'
			},
			audio_url: {
				type: jsPsych.plugins.parameterType.STRING,
				pretty_name: 'audio_URL',
				default: undefined,
				description: 'The url of the external audio page'
			},
			smil_url: {
				type: jsPsych.plugins.parameterType.STRING,
				pretty_name: 'smil_URL',
				default: undefined,
				description: 'The url of the external audio sync file page'
			},
			cont_key: {
				type: jsPsych.plugins.parameterType.KEYCODE,
				pretty_name: 'Continue key',
				default: null,
				description: 'The key to continue to the next page.'
			},
			cont_btn: {
				type: jsPsych.plugins.parameterType.STRING,
				pretty_name: 'Continue button',
				default: null,
				description: 'The button to continue to the next page.'
			},
			check_fn: {
				type: jsPsych.plugins.parameterType.FUNCTION,
				pretty_name: 'Check function',
				default: function() { return true; },
				description: ''
			},
			force_refresh: {
				type: jsPsych.plugins.parameterType.BOOL,
				pretty_name: 'Force refresh',
				default: false,
				description: 'Refresh page.'
			},
		}
	}

	plugin.trial = function(display_element, trial) {
		var url = trial.url
		if (trial.force_refresh) {
			url = trial.url + "?t=" + performance.now()
		}
		function init_audio(display_element, audio_url, smil_url){
			// create audio
			a = document.createElement("audio")
			let source = document.createElement("source")
			a.innerHTML = '<source src="' + audio_url + '" type="audio/mpeg">'
			display_element.appendChild(a)
			a.play()
			
			// assume html contains references to smil, spans have ID's like: PER-CH-1442
			// smil has PAR elements with <text src="009_Biodiversiteit_in_de_Ne.xhtml#CH-0001"><audio clipBegin="7.600"
			// load smil file with timings
			var xmlhttp = new XMLHttpRequest()
			xmlhttp.open("GET", smil_url, true)
			xmlhttp.overrideMimeType("text/xml") // no XML parsing without this line
			xmlhttp.onload = function(){
				if(xmlhttp.status == 200 || xmlhttp.status == 0)
					smil = xmlhttp.responseXML
			}
			xmlhttp.send()

			// react to clicks on spans
			var spans = document.querySelectorAll(".word")
			console.log("words: " + spans.length)
			for(let i=0; i<spans.length; i++){
				spans[i].addEventListener('click', function(e) {
					let el = smil.querySelector('text[src$=".xhtml#' + spans[i].id + '"]')
					let t = el.nextElementSibling.attributes['clipBegin'].value
					console.log("span"+" "+i+"/"+spans.length  + ", s: " + t + " / " + a.duration )
					//console.log(spans[i].getBoundingClientRect())
					a.currentTime = t
					if(a.paused)
						a.play()
					let data = getData()
					let click_data = { 
						x: e.clientX,
						y: e.clientY,
						i_word: i,
						n_word: spans.length,
						word: spans[i].textContent,
						trialSubPart: "click",
						currentTime: a.currentTime,
						duration: a.duration,
					}
					Object.assign(data, click_data)
					saveData(data) // merge
				}, false)
			}
		}

		var getDataCount = 0
		function getData(){
			let data = { 
				// from JsPsych default_data
				trial_type: trial.type,
				trial_index: jsPsych.progress().current_trial_global,
				time_elapsed: jsPsych.totalTime(),
				internal_node_id: jsPsych.currentTimelineNodeID() + "-" + getDataCount++ + ".0",
				// from external-html plugin
				rt: performance.now() - t0,
				url: trial.url,
				// extra
			}
			Object.assign(data, trial.data) // ppn, trialName, iBlock, iTrial, but not trialSubPart
			return data
		}
		load(display_element, url, function() {
			//let file = '/cool/static/cool.html?ppn=0&iOrder=0&intro=off&practice1=off&hash='+Math.random()
			//history.pushState('', 'Title of the page', file)
			//document.location.href = file
			t0 = performance.now()
			
			// send trigger
			let data = getData()
			Object.assign(data, trial.trigger)
			saveData(data) // merge
			
			// pause audio
			function space_listener(e){
				if(typeof a == 'undefined' || e.which!=32)
					return
				let space_data = getData()
				space_data['currentTime'] = a.currentTime
				space_data['duration'] = a.duration
 				if(a.paused){
					a.play()
					space_data['trialSubPart'] = "play"
				} else {
					a.pause()
					space_data['trialSubPart'] = "pause"
				}
				saveData(space_data)
			}
			document.addEventListener('keydown', space_listener) // 
			
			
			
			var finish = function() {
				if (trial.check_fn && !trial.check_fn(display_element))
					return 
				if (trial.cont_key) 
					display_element.removeEventListener('keydown', key_listener)
				document.removeEventListener('keydown', space_listener)
				var trial_data = {
					rt: performance.now() - t0,
					url: trial.url
				}
				display_element.innerHTML = ''
				jsPsych.finishTrial(trial_data)
			}

			if (trial.cont_btn)
				display_element.querySelector('#'+trial.cont_btn).addEventListener('click', finish)
			if (trial.cont_key) {
				var key_listener = function(e) {
					if (e.which == trial.cont_key) 
						finish()
				}
				display_element.addEventListener('keydown', key_listener)
			}
			if(trial.audio_url)
				init_audio(display_element, trial.audio_url, trial.smil_url)
			else 
				a = undefined
		})
	}

	// helper to load via XMLHttpRequest
	function load(element, file, callback){
		var xmlhttp = new XMLHttpRequest()
		xmlhttp.open("GET", file, true)
		xmlhttp.onload = function(){
			if(xmlhttp.status == 200 || xmlhttp.status == 0){ //Check if loaded
				element.innerHTML = "<div id='jspsych-html-audio'>" + xmlhttp.responseText + "</div>"
				callback()
			}
		}
		xmlhttp.send()
	}

	return plugin
})()
