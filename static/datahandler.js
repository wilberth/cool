/*
 * JavaScript function for saving and retrieving data
 * locally and on server.
 * 
 * Part of Krell.
 * (C) Wilbert van Ham Radboud University 2019
 * Released under GPLv3
 */ 
const data_id = "krell"   // prefix for keys in localStorage
//const data_url = "/data"  // url (path) for saving sata on server
//const data_url = "save_data_json.php"  // json style: {"ppn":"", other_data}
const data_url = "save_data_file.php"  // url style (post request): ppn=...&data={other_data}

// dictionaries send to 'addData' should contain a 'ppn' member identifying the participant
// dictionaries send to 'addData' should not contain a 'timestamp' member, since this is added by the save function


function loadPpns(){
	// load a dict with number of items per ppn
	var d = {}
	for (var i=0; i<localStorage.length; i++)
		if(localStorage.key(i).slice(0,data_id.length) == data_id){
			let [id, ppn, t] = localStorage.key(i).split("&")
			if(!(ppn in d))
				d[ppn] = [1, t, t] // first and last alteration time
			else { 
				d[ppn][0]++
				d[ppn][1] = Math.min(d[ppn][1], t)
				d[ppn][2] = Math.min(d[ppn][2], t)
			}
		}
	return d
}

function loadData(ppns){
	// load selection of localStorage items from array of dicts into array of arrays (list of rows)
	// selection is by list of ppn's
	// used by both 'showData' and 'download'
	let heads = new Set([])
	let rows = []
	for (var i=0; i<localStorage.length; i++)
		if(localStorage.key(i).slice(0,data_id.length) == data_id){
			let [id, ppn, t] = localStorage.key(i).split("&")
			if(ppns.includes(ppn)){ 
				let d = JSON.parse(localStorage.getItem(localStorage.key(i)))
				let row = []
				// add head if not yet there
				for(let head of Object.keys(d))
					heads.add(head)
				
				// add all values to row
				for(let head of heads)
					row.push(head in d ? d[head] : "")

				// add row to data 
				rows.push(row)
			}
		}
	
	// fill rows to make return value rectangular
	for(let row of rows)
		while (row.length < heads.size)
			row.push("")
	
			
	return [[...heads], rows] // spread operator converts Set to Array
}

function saveData(data){
	// save data
	let ppn = ""
	if("ppn" in data)
		ppn = encodeURIComponent(data['ppn'])
	
	// save locally
	let timestamp = new Date().valueOf()
	var key = data_id + "&" + ppn + "&" + timestamp
	localStorage.setItem(key, JSON.stringify(data))
	
	// send to server
	data["timestamp"] = timestamp
	var xhr = new XMLHttpRequest()
	xhr.open("POST", data_url, true)

	// json style:
	//xhr.setRequestHeader("Content-type","application/json")
	//var encodedData = JSON.stringify(data)
	// url (post) style:
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	var encodedData = "ppn=" + ppn + "&data=" + encodeURIComponent(JSON.stringify(data))

	xhr.send(encodedData)
	xhr.onreadystatechange = function(){
		if (xhr.readyState==4 && xhr.status==200){
			try {
				var r = JSON.parse(xhr.responseText)
			} catch(e) {
				alert("no valid server response: " + xhr.responseText)
				return
			}
			console.log("Successfully uploaded data: " + xhr.responseText);
		}
	}
}
